Payment Module  : ec_paperpayments
Original Author : Dublin Drupaller
Settings        : administer > eCommerce configuration > receipt types > Cheque/Bank draft/Postal Order

********************************************************************
DESCRIPTION:

This module allows you to accept and track Cheque/Bank Drafts/Postal Order
methods of payment on your Drupal eCommerce shop.

Contact Dublin Drupaller via http://www.DublinDrupaller.com for assistance
or for suggestions, ideas or improvements.
********************************************************************


INSTALLATION
------------------
This version of the ec_paperpayments.module requires version 6.x-4.x of the Drupal eCommerce API

Pre-installation note: After downloading the module from Drupal.org, it is recommended you
upload the ec_paperpayments module files to /sites/all/modules/ecommerce/ec_paperpayments

(a) Enable the module as you would any other Drupal module under ADMINISTER -> SITE BUILDING -> MODULES

(b) Once enabled go to the ec_paperpaymentse settings page which can be found by following these links

ADMINISTER > E-COMMERCE CONFIGURATION -> RECEIPT TYPES ->

(c) You must save your configuration settings prior to making the payment option available.


UNINSTALL
-------------

Uninstall routine is included, ADMINISTER -> SITE BUILDING -> MODULES


NOTES
---------
For support/assistance, or if you have any ideas for improvements, please
contact Dublin Drupaller: dub@dublindrupaller.com